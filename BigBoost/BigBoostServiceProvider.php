<?php


namespace BigBoost;

use BigBoost\BigBoostBase;
use BigBoost\Lib\BigBoostPeoples;
use Illuminate\Support\ServiceProvider;
use BigBoost\Lib\BigBoostCompanies;
use BigBoost\Lib\BigBoostGetMethods;

class BigBoostServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot()
    {

    }

    /**
     * @return void
     */
    public function register() {
        $this->app->singleton('BigBoostCompanies',function() {
            return new BigBoostCompanies();
        });
        $this->app->singleton('BigBoostPeoples',function() {
            return new BigBoostPeoples();
        });
        $this->app->singleton('BigBoostGetMethods',function() {
            return new BigBoostGetMethods();
        });
    }
    /**
     * @return array
     */
    public function provides()
    {
        return [
            BigBoostCompanies::class,
            BigBoostPeoples::class,
            BigBoostGetMethods::class,
        ];
    }
}
