<?php


namespace BigBoost\Contracts;


interface BigBoostCompanies
{
    /**
     * Responsible for obtaining a PJ reputation
     *
     * @param string $taxpayer
     */
    public function reputation($taxpayer);
    /**
     * Responsible for obtaining all first level partners
     *
     * @param string $taxpayer
     */
    public function partners($taxpayer);
    /**
     * Responsible for obtaining all employees information
     *
     * @param string $taxpayer
     */
    public function employees($taxpayer);
    /**
     * obtain a simple informations of companie
     *
     * @param string $taxpayer
     */
    public function basicData($taxpayer);
}
