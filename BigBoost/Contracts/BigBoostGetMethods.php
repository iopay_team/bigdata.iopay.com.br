<?php


namespace BigBoost\Contracts;


interface BigBoostGetMethods
{
    /**
     * Get List of all BigBoost methods
     *
     */
    public function getAll();
    /**
     * Get List of all Peoples BigBoost methods
     *
     */
    public function getPeoples();
    /**
     * Get List of all Companies BigBoost methods
     *
     */
    public function getCompanies();
}
