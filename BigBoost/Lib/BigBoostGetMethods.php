<?php


namespace BigBoost\Lib;

use BigBoost\BigBoostBase;

class BigBoostGetMethods implements \BigBoost\Contracts\BigBoostGetMethods
{
    /**
     * Get List of all BigBoost methods
     *
     */
    public function getAll()
    {
        $peoples    = $this->getPeoples();
        $companies  = $this->getCompanies();
        $list = array_merge($peoples,$companies);
        return $list;
    }

    public function getPeoples()
    {

        $list = [
            'kyc' => [
                'name'=> 'Know Your Customer',
                'value'=> 'kyc'
            ],
            'election_candidate_data' => [
                'name' => 'Candidatos Eleitorais',
                'value'=> 'election_candidate_data'
            ],
            'media_profile_and_exposure' => [
                'name' => 'Exposição e Perfil na Mídia',
                'value'=> 'media_profile_and_exposure'
            ],
            'financial_data' => [
                'name' => 'Informações Financeiras',
                'value'=> 'financial_data',
            ],
        ];
        return $list;
    }

    public function getCompanies()
    {
        $list = [
            'circles_employees' => [
                'name' => 'Circulo de  Funcionários',
                'value'=> 'circles_employees',
            ],
            'basic_data' => [
                'name' => 'Dados Básicos',
                'value'=> 'basic_data',
            ],
            'relationships' => [
                'name' => 'Sócios de Primeiro Nível',
                'value'=> 'relationships',
            ],
            'reputations_and_reviews' => [
                'name' => 'Reputação de PJ',
                'value'=> 'reputations_and_reviews',
            ],
        ];
        return $list;
    }
}
