<?php


namespace BigBoost\Lib;

use BigBoost\BigBoostBase;

class BigBoostPeoples implements \BigBoost\Contracts\BigBoostPeoples
{
    public function kyc($taxpayer)
    {
        $api = 'peoplev2';

        $params = [
            "Datasets"  => "kyc",
            "q"         => "doc{".$taxpayer.'}',
        ];
        return BigBoostBase::getCurl($api,$params);
    }

    public function getDataset($dataset, $document){
        $api = 'peoplev2';
        $params = [
            "Datasets"  => $dataset,
            "q"         => "doc{".$document.'}',
        ];
        return BigBoostBase::getCurlWithAuthParams($api,$params);
    }

}
