<?php


namespace BigBoost\Lib;

use BigBoost\BigBoostBase;
use PhpParser\Node\Scalar\String_;

class BigBoostCompanies implements \BigBoost\Contracts\BigBoostCompanies
{
    /**
     * Create any Big Data Companies Request
     * @param String $api
     * @description Type of api ('companies','peoplev2')
     * @param String $dataset
     * @description Type Request
     * @param string $taxpayer
     * @description TaxpayerID
    */
    public function BigCompanie($api,$dataset,$taxpayer)
    {
        $params = [
            "Datasets"  => $dataset,
            "q"         => "doc{".$taxpayer.'}',
        ];
        return BigBoostBase::getCurlWithAuthParams($api,$params);
    }

    public function getDataset($dataset, $document){
        return $this->BigCompanie("companies", $dataset, $document);
    }

    /**
     * Responsible for obtaining a PJ reputation
     *
     * @param string $taxpayer
     */
    public function reputation($taxpayer)
    {
        $api = 'companies';


        $params = [
            "Datasets"  => "reputations_and_reviews",
            "q"         => "doc{".$taxpayer.'}',
        ];

        return BigBoostBase::getCurl($api,$params);
    }
    /**
     * Responsible for obtaining all first level partners
     *
     * @param string $taxpayer
     */
    public function partners($taxpayer)
    {
        $api = 'companies';


        $params = [
            "Datasets"  => "relationships",
            "q"         => "doc{".$taxpayer.'}',
        ];

        return BigBoostBase::getCurl($api,$params);
    }
    /**
     * Responsible for obtaining all employees information
     *
     * @param string $taxpayer
     */
    public function employees($taxpayer)
    {
        $api = 'companies';


        $params = [
            "Datasets"  => "circles_employees",
            "q"         => "doc{".$taxpayer.'}',
        ];

        return BigBoostBase::getCurl($api,$params);
    }
    public function basicData($taxpayer)
    {
        $api = 'companies';


        $params = [
            "Datasets"  => "basic_data",
            "q"         => "doc{".$taxpayer.'}',
        ];
        return BigBoostBase::getCurl($api,$params);
    }
}
