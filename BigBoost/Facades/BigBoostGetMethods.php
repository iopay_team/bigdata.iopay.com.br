<?php

namespace BigBoost\Facades;

use Illuminate\Support\Facades\Facade;

class BigBoostGetMethods extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'BigBoostGetMethods';
    }
}
