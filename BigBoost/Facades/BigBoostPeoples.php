<?php

namespace BigBoost\Facades;

use Illuminate\Support\Facades\Facade;

class BigBoostPeoples extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'BigBoostPeoples';
    }
}
