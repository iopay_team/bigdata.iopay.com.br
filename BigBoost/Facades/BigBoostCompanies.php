<?php

namespace BigBoost\Facades;

use Illuminate\Support\Facades\Facade;

class BigBoostCompanies extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'BigBoostCompanies';
    }
}
