<?php

namespace BigId;

class BigidBase{

    /**
     * CURL config params
     * @param null $key if "key" requested: returns value or null if not exists; if "key" not requested or null, returns all array with config params
     * @return array|mixed|null
     */
    private static function config($key = null){
        $config = [
            "api_url" => 'https://bigid.bigdatacorp.com.br/', // URL base da API BigID terminado com "/"
            "headers" => [ // CURL headers
                'Authorization: Bearer '. env('BIG_DATA_KEY'),
                'Content-Type: application/json'
            ],
            "token" => env('BIG_DATA_KEY'),
            "login" => env('BIG_DATA_EMAIL_LOGIN', 'rodrigo.rodriguez@iopay.com.br')
        ];

        // if "key" requested: returns value or null if not exists
        if (!empty($key)){ return $config[$key] ?? null; }

        // if "key" not requested or null, returns all array with config params
        return $config;
    }

    public static function getCurlWithAuthParams(string $api_resource_path, array $params = [], string $method = "POST"){
        // Additional auth params
        $params["AccessToken"] = self::config("token");
        $params["Login"] = self::config("login");

        return self::getCurl($api_resource_path, $params, $method);
    }

    /**
     * @param string $api_resource_path
     * @param array $params
     * @param string $method Default POST; Methods: GET, POST, DELETE, PATCH, PUT [...]
     * @return bool|string curl response raw
     */
    public static function getCurl(string $api_resource_path, array $params = [], string $method = "POST"){
        $method = strtoupper(trim($method));

        $curl = curl_init();

        // default curl options
        $curl_opts = array(
            CURLOPT_URL => self::config("api_url") . $api_resource_path,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => self::config("headers"),
        );

        if ($method  == "GET" && !empty($params)){ //requisição GET (inserção dos parâmetros na URL)
            $curl_opts[CURLOPT_URL] .= "?" . http_build_query($params);
        }else{ // requisições PST, PUT, DELETE ... (inserção dos parâmetros no corpo da requisição)
            $curl_opts[CURLOPT_POSTFIELDS] = json_encode($params);
        }

        curl_setopt_array($curl, $curl_opts);

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

}
