<?php

namespace BigId\Facades;

use Illuminate\Support\Facades\Facade;

class BackgroundCheck extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'BackgroundCheck';
    }
}
