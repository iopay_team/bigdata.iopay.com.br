<?php


namespace BigId;

use BigId\BigidBase;
use Illuminate\Support\ServiceProvider;
use BigId\Lib\BackgroundCheck;

class BigidServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot()
    {

    }

    /**
     * @return void
     */
    public function register() {
        $this->app->singleton('BackgroundCheck',function() {
            return new BackgroundCheck();
        });
    }
    /**
     * @return array
     */
    public function provides()
    {
        return [
            BackgroundCheck::class,
        ];
    }
}
