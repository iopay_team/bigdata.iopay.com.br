<?php


namespace BigId\Contracts;


interface BackgroundCheck
{
    /**
     * Retrieve the details of a Seller by taxpayer
     * @param $api string
     * @param $taxpayer array
     */
    public function check($taxpayer);

    public function questions($taxpayer);

    public function answers($TicketId,$answers);

    public function getAnswers($TicketId);
}
