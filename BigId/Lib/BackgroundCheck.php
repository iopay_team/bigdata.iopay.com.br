<?php


namespace BigId\Lib;

use BigId\BigidBase;

class BackgroundCheck implements \BigId\Contracts\BackgroundCheck
{
    /**
     * Responsible for obtaining a personal score whether she is a PF or PJ
     *
     * @param string $taxpayer
     */
    public function check($taxpayer)
    {
        $api = 'backgroundcheck';

        $taxpayer = str_replace(".","",$taxpayer);
        $taxpayer = str_replace("-","",$taxpayer);
        $taxpayer = str_replace("/","",$taxpayer);
        if(strlen($taxpayer) < 12) {
            $parameters = ["CPF" => $taxpayer];
        } else {
            $parameters = ["CNPJ" => $taxpayer];

        }
        $params = [
            "Login"         => "rodrigo.rodriguez@iopay.com.br",
            "parameters"    => $parameters,
        ];

        return BigidBase::getCurl($api,$params);
    }

    /**
     * Responsible for obtaining a personal form questions whether she is a PF or PJ
     *
     * @param string $taxpayer
     */

    public function questions($taxpayer)
    {
        $api = 'Questions/DisplayLink';
        $taxpayer = str_replace([",",".","-","_"," ","/","\\"],"",$taxpayer);
        $params = ["parameters"    => ["CPF=$taxpayer"]];

        return BigidBase::getCurl($api,$params);
    }

    /**
     * Responsible for obtaining a personal answers with a ticketid
     *
     * @param string $ticketId
     */

    public function answers($TicketId,$answers)
    {
        $api = 'Answers';
        $params = [
            "TicketId"      => $TicketId,
            "Parameters"    => $answers,
        ];

        return BigidBase::getCurl($api,$params);
    }

    public function getAnswers($TicketId)
    {
        $api = 'Events';
        $params = [
            "TicketId"      => $TicketId,
        ];

        return BigidBase::getCurl($api,$params);

    }
}
